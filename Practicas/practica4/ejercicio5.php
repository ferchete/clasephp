<?php

$a=[
    "Lunes"=>100,
    "Martes"=>150,
    "Miercoles"=>300,
    "Jueves"=>10,
    "Viernes"=>50
];

$b="Miercoles";

if(array_key_exists($b, $a)){
    echo $a[$b];
}else{
    echo "No se";
}

/*
 * El programa devuelve el valor FALSE
 * Sin embargo, si invertimos los valores del array_key, nos sale 300
 * Sale error porque en el array_key_exists, el indice tiene que ser siempre 
 * el primer valor.
 */




?>
