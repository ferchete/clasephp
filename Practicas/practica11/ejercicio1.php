<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
    </head>
    <body>
        <?php
        /**
         * Generamos numeros aleatorios
         * @param int $max valor minimo
         * @param int $min valor maximo
         * @param int $num Numero de valores a generar
         * @return type
         */
        
        function ejercicio1($min, $max, $num){
            $local = array(); //array donde colocar el resultado
 
       
         /* 
         * Bucle para rellenar el array
         */        
        
       for($c=0;$c<$num;$c++){ //rellenamos el array con el FOR
         $local[$c]= mt_rand($min, $max);
        }
       
        return $local;
        
        
        }
        
        /*
         * Devolver el array con el var_dump
         */
        
        $salida=ejercicio1(1,10,10);
        var_dump($salida);
        
        
        ?>
        
    </body>
</html>
