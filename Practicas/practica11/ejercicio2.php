<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
      <?php
        /**
         * Generamos numeros aleatorios
         * @param int $max valor minimo
         * @param int $min valor maximo
         * @param int $num Numero de valores a generar
         * @param int 
         * @return type
         */
        
        function ejercicio2($min, $max, $num, &$salida){
           
           //Inicializando el array para que no se quede con los valores anteriores
            
            $salida = array(); //array donde colocar el resultado
 
       
         /* 
         * Bucle para rellenar el array
         */        
        
       for($c=0 ;$c < $num; $c++){ //rellenamos el array
           
         $salida[$c]= mt_rand($min, $max);
       
       
        
         }
        
        }
      /*  $numeros=[25,50];
       *
       * ejercicio2(7,100,8,$numeros); 7=numero minimo | 100 = numero maximo | 8 = numero valores a generar | $numeros el array vacio donde lo quiero almacenar
        
       * var_dump($numeros);
        
       *function sumar($numeros){
            $resultado=0;
            foreach($numeros as $numero){
                $resultado=$resultado+$numero;
            }
            return $resultado;
        }
        
        
        $datos=[3,5];
        $salida=0;
        $salida=sumar($datos);
        *var_dump($salida,$datos);
       * 
       */
        ?>
    </body>
</html>
