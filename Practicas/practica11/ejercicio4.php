<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 4</title>
    </head>
    <body>
        <?php
        /**
         * Generar colores 
         * @param int $numero numero de colores a generar
         * @param bool $almohadilla=true con valor true nos indica que coloquemos la almohadilla
         * @return array de los colores solicitados en un array
         */
        function generaColores($numero, $almohadilla=true){ //creamos la función
            $colores=array();
            for($n=0;$n<$numero;$n++){ //inicializamos el bucle
                $c=0;
                $limite=6;
                $colores[$n]="";
                //Colocamos la amohadilla mediante un IF
                if($almohadilla){
                    $colores[$n]="#";
                    $limite=7;
                }
                
                for(;$c<$limite;$c++){ //Con este bucle generamos los numeros restantes del color
                    $colores[$n].= dechex(mt_rand(0,15)); // con dechex transformamos numeros enteros a hexadecimales
                }
            }
            return $colores; //Lo que queremos que nos retorne
        }
     // $colores=generaColores(10); || Le decimos los valores que nos tiene que mostrar de la funcion
     //var_dump($colores);
        
        
        ?>
    </body>
</html>
