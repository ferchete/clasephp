<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        /**
         * Funcion que te permite generar un array con el contenido de un directorio
         *  La salida esta ordenada
         * @param type $handle cadena de caracteres con la ruta del directorio a listar
         * @return string[] array con el nombre de los ficheros y directorios
         */
        function leerDirectorio($handle = ".") {
            $handle = opendir($handle);
            while (false !== ($archivo = readdir($handle))) {
                $archivo[] = strtolower($archivo);
            }
            closedir($handle);
            sort($archivos);
            return $archivos;
        }

        //var_dump(leerDirectorio());
        //var_dump(leerDirectorio(".")); //lee el directorio en que se encuentra
        //var_dump(scandir("..")); //lee el directorio anterior a donde se encuentra
        ?>
    </body>
</html>
