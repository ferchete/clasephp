<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 5</title>
    </head>
    <body>
        <?php
        /**
 * Funcion que le pasa un array con valores y ella te devuelve las veces que se repite cada valor
 * @param int $array es el conjunto de valores a utilizar
 * @param int $devolverTodos si le indicas true te devuelve los valores que no se repiten tambien
 * @return int[] es el array con las frecuencias de cada uno de los valores de array de entrada
 */

        function elementosRepetidos($array, $devolverTodos = false){
            $repeated = array();
            foreach((array) $array as $value){
                $inArray = false;
                foreach ($repeated as $i =>$rItem){
                    if($rItem['value'] === $value){
                        $inArray = true;
                        ++$repeated[$i]['count'];
                    }
                }
                if (false=== $inArray){
                    $i = count($repeated);
                    $repeated[$i] = array ();
                    $repeated[$i]['value'] = $value;
                    $repeated[$i]['count'] = 1;
                }
            }
            if(!$devolverTodos){
                foreach($repeated as $i => $rItem){
                    if($rItem['count'] === 1){
                        unset($repeated[$i]); //unset para romper las variables especificadas
                    }
                }
            }
            sort($repeated);
            
            return $repeated;
        }
        
        
        
        
        
        
        
        ?>
    </body>
</html>
