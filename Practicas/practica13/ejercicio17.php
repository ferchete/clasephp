<!DOCTYPE html>
<?php

//Crear una función que le pasas un string como argumento y te devuelve el numero de vocales que tiene el string. 

/*
 * ejercicio17("ejemplo de clase"); -> Tendría que devolver el numero
 * function ejercicio17($texto){
 *  -> opcion 1: mediante un for recorremos el string y comprobamos que el caracter es una vocal
 *  ->opcion 2: convierte el string en un array(str_split) y recorremos el array con un foreach y compruebas si cada caracter es una vocal(if)
 *  ->opcion 3: funciones propias de php
 * }
 */


//inicializamos la funcion

/*
 *              Recorrer la cadena desde 0 hasta su longitud menos 1 (por eso usamos <)
 *		Ponemos la longitud dentro de una variable para evitar
 *		múltiples llamadas a strlen() en cada iteración
 */


function cuantasVocales($textazo){
    
    $vocales = 0;
    
    // Inicializamos el bucle
    
    for($c=0;$c<$prueba = strlen($textazo);$c++){
        
       // preguntamos con un if, dentro de una funcion propia las coincidencias
        
    if (in_array($textazo[$c], ["a", "e", "i", "o", "u"])) {
        
      //  Contamos los elementos
        
			$vocales++;
		}
	}
        
       // Devolvemos la funcion donde hemos almacenado los resultados
        
	return $vocales;
}

//Funcion propuesta en clase con switch y con el mb 
    function ejercicio17($texto1){
        
        $vocales = 0;
  
    for($c=0;$c < strlen($texto1);$c++){
        
        
        //switch (strlen($texto,$c,1)) --> para cuando queramos contar mas de un caracter
        
        switch (mb_strtolower(mb_substr($texto1,$c,1))) {
            case "a":
            case "á":    
            case "e":
            case "é":   
            case "i":
            case "í":    
            case "o":
            case "ó":    
            case "ú":    
            case "u":
                
                $vocales++;
         

        }
        
    }
    //retornamos la funcion
      return $vocales;  
    }

    
    function ejercicio17v1($texto) {
        
            $nvocales = 0;
            
            $vocales=["a", "e", "i", "o", "u","á","é","í","ó","ú"];
            
            for($c=0;$c<strlen($texto);$c++){
                
                if (in_array(mb_strtolower(mb_substr($texto,$c,1)),$vocales)){
                    
                    $nvocales++;
                    
                }
        }
        
    echo $nvocales;
}





?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 17</title>
    </head>
    <body>
        <?php
        
        $textazo = "Yo no entiendo nada";
        
        $texto = "Efectos vocales, yé";
        
        $texto1 = "Ejemplo de clase de Ramón";
        
        
        
       // echo cuantasVocales($texto);
        
       // echo ejercicio17($texto1);
        
     
        ?>
        
       
        
        <p><h1><?= ejercicio17($texto1)?></h1></p>
        
        <p><h2><?= ejercicio17v1($texto)?></h2></p>

        <p><h3><?= cuantasVocales($textazo)?></h3></p>
        
        
    </body>
</html>
