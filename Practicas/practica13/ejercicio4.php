<!DOCTYPE html>
<?php

$num1 = mt_rand(1,5);

$num2 = mt_rand(3,7);

$num3 = mt_rand(5,9);

function suma($num1, $num2){
    
     return $num1 + $num2;
    
    
}

function suma3($num1, $num2, $num3){
    
     return $num1 + $num2 + $num3;
    
    
}

//Podemos utilizar argumentos opcionales para solo hacer una funcion

/*
 * Otro ejemplo
 * function ejercicio4($numero1=0,$numero2=0,$numero3=0){
 * $resultado = $numero1 + $numero2 + $numero3;
 * echo "<br> {$resultado} <br>";
 * }
 */
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 4</title>
    </head>
    <body>
        <?php
        
        //imprimo los resultados
       echo "Mis números son $num1 y $num2 y su suma es " . suma($num1,$num2) . "<br>";
       
       echo "Mis números son $num1, $num2, $num3 y su suma es " . suma3($num1, $num2, $num3);
        
       //Llamando a la ultima funcion
       //ejercicio4(5,7);
       //ejercicio4(5,7,1);
       //ejercicio4(); Para ver que hace, simplemente
       ?>
    </body>
</html>
