<!DOCTYPE html>
<?php

/*
 * Crear  una  variable  global  denominada  resultado.  Crear  una  función  que  le  pasas  2  numeros  y  te  almacena  el 
resultado en la variable global resultado. 
 */

function ejercicio16 ($numero1,$numero2){
   
    global $resultado; //practica desaconsejable
    
    $resultado = $numero1 + $numero2;
    
    
    
}
//inicializamos las funciones
function ejercicio16singlobal ($numero1,$numero2){
    
    return $numero1 + $numero2;
  
}



?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 16</title>
    </head>
    <body>
        <?php
        
        $resultado = 0; 
        
       ejercicio16(2,3);
       
       echo $resultado;
       
       //Llamamos a la función
       
       $resultado = ejercicio16singlobal(2, 3);
       
       //Imprimimos la funcion
       
       echo "<br>" . $resultado;
        ?>
    </body>
</html>
