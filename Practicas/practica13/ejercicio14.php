<!DOCTYPE html>
<?php
//Crear una función que le pasas n números (como un array) y te devuelve la suma de todos ellos


//                  argumento
function sumarray($numarray){
    
    $resultado = 0; //inicializamos la variable
    
    $resultado = array_sum($numarray); // Asignamos el valor a retornar a resultado
    
    return $resultado; 
    
}

function sumafor($numFor){
    
    $resultado = 0;  //acumulador
    
    for($contador = 0; $contador < count($numFor);$contador++){
        
        $resultado = $resultado + $numFor[$contador];
        
//1º iteracion
//$contador = 0
//$resultado = 21
//
//2º iteracion
//$contador = 1
//$resultado = $resultado + 20 = 21 + 20 = 41
//
//3º iteracion
//$contador = 2
//$resultado = $resultado + 22 = 41 + 22 = 63
    }
    return $resultado;
}

function ejercicio14($numeros){
    $resultado = 0;
    foreach ($numeros as $numero){
        $resultado = $resultado + $numero;
    }
    return $resultado;
}



?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 14</title>
    </head>
    <body>
        <?php
        
        $numerazo = [21,20,22];
        
        
       // echo sumarray($numerazo) . "<br>"; 
        
        //echo sumafor($numerazo)
        
        
        
        ?>
        
        <p><b><?= sumafor($numerazo)?></b></p>
        
        <p><i><?= sumarray($numerazo)?></i></p>
        
        <p><b><i><?= ejercicio14($numerazo)?></i></b></p>
        
        
    </body>
</html>
