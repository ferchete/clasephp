<!DOCTYPE html>
<?php

//Inicializamos las variables con numeros aleatorios
$num1 = mt_rand(1,10);

$num2 = mt_rand(1,10);

$num3 = mt_rand(1,10);

//Inicializamos la funcion

function producto($num1, $num2,$num3){
    
    //La retornamos
     return $num1 * $num2 * $num3;
    
    
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 5</title>
    </head>
    <body>
        <?php
        
        ///Imprimimos el resultado
       echo "Mis números son $num1, $num2 y $num3 cuyo producto es " . producto($num1,$num2,$num3);
        ?>
    </body>
</html>
