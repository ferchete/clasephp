<!DOCTYPE html>
<?php

    function getarray($numeros){
        
        //Contador de numeros negativos
        
        $n = 0;
        
        
        //Recorremos el array con la funcion count desde el indice 0 hasta el final del arrray
        //<= count($numeros)-1
        
        
       for($c = 0; $c < count($numeros); $c++ ){
           
           
           
           if($numeros[$c] < 0){
               
               //Si cada numero es menor que 0, el contador $n suma
               
               $n++;
               
           }
           
           
       }
        //Si existen numeros negativos en el array te da mensaje de negativos y sino, positivos
       if($n > 0 ){
           
           echo "hay negativos";
       }else{
           
           echo "son positivos";
           
       }
        
    }
    
    //
    //Esta solucion es mas limpia y mas simple
    //
//    recorremos el array con el foreach
//    
//    foreach($numeros as $valor){
//        
//            
//        if($valor < 0){
//            
//            return "hay negativos";
//        }
//    }
//    return "hay positivos";
//    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 9</title>
    </head>
    <body>
        <?php
        
        
        $numeros = [-2,-1,0,1,2];
        
        
        
        echo getarray($numeros);
        
        ?>
    </body>
</html>
