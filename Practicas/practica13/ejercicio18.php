<!DOCTYPE html>
<?php
/*
 * Crear una función que le pasas un string como argumento y te devuelve un array con las vocales del string. 
 */

function efectosVocales($texto){
    
    $vector= str_split($texto); // Me crea un array con un string
    
    $vocales=[]; //Creamos el array vacio para introducir los datos
    
    foreach($vector as $value){ //Recorremos el array con el foreach
        
        if($value == "a" or $value == "A"){ //Le decimos que nos encuentre las coincidencias de $texto con esas condiciones
            
            $vocales[] = $value; //Las introducimos en el array
            
        }elseif($value == "e" or $value == "E"){
            
            $vocales[] = $value;
            
        }elseif($value == "i" or $value == "I"){
            
            $vocales[] = $value;
            
        }elseif($value == "o" or $value == "O"){
            
            $vocales[] = $value;
            
        }elseif($value == "u" or $value == "U"){
            
            $vocales[] = $value;
            
        }
        
    }
   return $vocales; //La retornamos con el array donde hemos introducido los datos
}








?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 18</title>
    </head>
    <body>
        <?php
        
        $texto = "Efectos vocAles";
        
         $vocales = efectosVocales($texto);
         
         echo efectosVocales($texto);
         
         
        
        var_dump($vocales);
        
        ?>
    </body>
</html>
