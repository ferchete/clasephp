<!DOCTYPE html>
<?php 


// Creamos una funcion para poner el string al reves 

function insideOutText($palabro1){
    
    
   //La retornamos 
    
    //Me coloca el texto al reves
    return strrev($palabro1);
    
}

/*
 * Version en la que se convierte el string en un array con la funcion
 * str_split
 * @param type $texto
 */


function ejercicio12($texto){
    
    $vector= str_split($texto); // Me crea un array con un string
    //strlen($texto) //-> me cuenta los caracteres del texto
    //count($texto) //-> Me cuenta los caracteres del string
    //array_reverse($texto) //->Me da la vuelta al string
    //implode("", $vector) //Me lo vuelve a convertir en un string, las comillas son el caracter a unir
    for($c = count($vector)-1;$c >=0;$c--){
        echo $vector[$c];
    }
    
}
/**
 * Version que muestro el exto girado tratrando el texto como un array
 * EL FOREACH NO
 * @param type $texto
 */

function ejercicio12v2($texto1){
    for($c= strlen($texto1)-1;$c>=0;$c--){
        echo $texto1[$c];
    }
}






?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 12</title>
    </head>
    <body>
        <?php
        
        //Creamos la variable donde almacenamos el texto
        
        $palabro1 = " Facta, non verba. ^_____^ Hechos, no palabras. ";
        
        $texto =  "Manzanas";
        
        $texto1= "Un texto a girar";
        
       
       // Imprimimos el texto en su forma correcta 
        echo " <br> $palabro1 <br> ";
        
       //mostramos la funcion que hemos creado para que nos imprima la palabra al reves
        
        echo insideOutText($palabro1);
       
        ?>
      
        
        <p><em><?=   ejercicio12($texto)?></em></p>
        
        <p><b><?=   ejercicio12v2($texto1)?></b></p>
        
        
    </body>
</html>
