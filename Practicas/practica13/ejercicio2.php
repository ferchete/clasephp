<!DOCTYPE html>
<?php
//Iniciamos la función

function imprimeCien() {

    //Iniciamos la variable, que empiece en 1
    $contador = 1;
    
    //Iniciamos el bucle While,para que sea igual o menor que 100

    while ($contador <= 100) {

        //Imprimimos los resultados
        echo $contador;
        echo "<br>";
        
        //Le decimos a la variable que vaya de uno en uno
        $contador++;
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 2</title>
    </head>
    <body>
        <?php
        
        //Llamamos a la función
        
        imprimeCien();
        ?>
    </body>
</html>
