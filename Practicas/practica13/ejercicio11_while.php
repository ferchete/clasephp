<!DOCTYPE html>
 <?php
 
        // Inicialización: $c = tamaño del array - 1
        // 
        // Condición: $c es mayor o igual que 0
        // 
        // Iteración: $c = $c - 1
 
 function insideOutConWhile($a){
     
     $c = count($a) - 1;
     
     while($c >= 0){
         
         echo $a[$c];
         echo"<br>";
         
         $c--;
         
     }
 }
 //Con un foreach
//    function ejercicio11($a1){
//        $datosReversibles = array_reverse($a1);
//        foreach ($datosReversibles as $valor){
//            echo "$valor <br>";
//        }
//    }
// 
 ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CON WHILE</title>
    </head>
    <body>
        <?php
        $a=["Hasta luiegui",1,2,3,4,5,6,7,8,9,"Hola que tal"];
        
     //   $a1=[1,2,3,4,5,6,7,8,9];
        
         insideOutConWhile($a);
         
       //  ejercicio11($a1);
         
         
        ?>
    </body>
</html>

<?php
//$a = [1,2,3,4,5,6,7,8,9]
//
//es equivalente a:
//
//$a = [
//	0 => 1,
//	1 => 2,
//	2 => 3,
//	3 => 4,
//	4 => 5,
//	5 => 6,
//	6 => 7,
//	7 => 8,
//	8 => 9
//]
//
//La primera columna es el índice (la que empieza en 0), la segunda es el valor (empieza en 1)
//tú cuando muestras el array lo haces así:
//
//$c = count($a) - 1 ===> $c = 9 - 1 ===> $c = 8
//
//echo $a[$c] ===> echo $a[8] ===> echo 9

?>