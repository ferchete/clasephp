<!DOCTYPE html>
<?php

//Inicializamos la función

function imprimeDiez() {
    /*
     * Inicializamos el bucle FOR, para que empiece desde 1
     * después, que llegue hasta menor o igual que 10
     * Que vaya de 1 en 1
     *
     */

    for ($contador = 1; $contador <= 10; $contador++) {
        
        //Imprimimos el resultado
        
        echo $contador;
        echo "<br>";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
    </head>
    <body>
        <div align="center">
<?php
//Llamamos a la función
imprimeDiez();
?>
            </div>
    </body>
</html>
