<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $alumno1 = "Ramón";
            $alumno2 = "Jose";
            $alumno3 = "Pepe";
            $alumno4 = "Ana";
            
            echo $alumno1;
            echo $alumno2;
            echo "<br>";
            echo $alumno3;
            echo "<br>";
            echo $alumno4;
            
            //Imprime los valores de las variables
            /*RamónJose
             * Pepe
             * Ana 
             */
        ?>
        <div>
            <?php
            echo "$alumno1 <br> $alumno2 <br> $alumno3 <br> $alumno4";
            /*Nos imprime de esta manera las variables
             * Ramón 
             * Jose
             * Pepe
             * Ana
             */
            
            ?>
            
        </div>
    </body>
</html>
