

<?php
/*
 * Recogemos los datos del formulario mediante GET 
 * y hacemos la comparación con un ELSEIF
 * Realizamos las comparaciones del primer número con el segundo y tercero
 * después el segundo con el primero o tercero
 * y por último, si no se da ningun caso restante, el mayor es el tercer número.
 */

$num1 = $_GET["numero1"];
$num2 = $_GET["numero2"];
$num3 = $_GET["numero3"];

if ($num1 >= $num2 && $num1 >= $num3) {

    echo "<h1>$num1 es mayor</h1>";
} elseif ($num2 >= $num1 && $num2 >= $num3) {
    echo "<h1>$num2 es mayor</h1>";
} else {
    echo "<h1>el $num3 es mayor</h1>";
}
?>
