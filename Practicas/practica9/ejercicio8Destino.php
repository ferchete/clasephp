<?php

//Recogemos los datos del formulario
$radio=$_GET["radio"];

//Realizamos las operaciones con las fórmulas
$area=pi()*($radio**2);
$longi=pi() * ($radio*2);
$esfer=(4/3)* pi() * ($radio**3);

//Redondeamos los números para que no sean tan largos
$area1= round($area, 2);
$longi1= round($longi, 2);
$esfer1= round($esfer, 2);

//Imprimimos los resultados
echo "<font size=8>La fórmula del area es: <u>π · r<sup>2</sup> </u> =  $area1 </font><br>";
echo "<font size=10>La fórmula de la longitud es: <u> π · r<sup>2</sup></u> = $longi1</font><br>";
echo "<font size=12>La fórmula del volumen de la esfera es: <u>(4/3) · π · r<sup>3</sup></u> = $esfer1</font><br>";





?>