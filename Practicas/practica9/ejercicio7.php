<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 7</title>
    </head>
    <body>
        <form action="ejercicio7Destino.php" method="GET">
            <div>
            <label for="numero1">
                Introduce un número si quieres que te lo sume
                <input type="text" name="numero1" placeholder="Si no quieres nada" required>
            </label>
            </div>
            <br>
            <div>
            <label for="numero2">
                Introduce otro número y lo sumo, venga
                <input type="text" name="numero2" placeholder="Venga, que ya está casi" required>
            </label>
            </div>
            <br>
            Y ahora, vamos a realizar unas cuantas operaciones
            <br>
            <br>
            <input type="submit" value="Hasta el infinito, y mas allá!">
        </form>
        
    </body>
</html>
