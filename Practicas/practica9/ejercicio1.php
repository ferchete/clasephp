<!DOCTYPE html>
<!--
Introducimos unos datos determinados y los recogemos en la siguiente página mediante el metodo GET.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
    </head>
    <body>
        <form action="ejercicio1Destino.php" method="GET">
            
            <table class="default" width="350">
     
                    
            
                <tr>   
                    <td>
                        <label for="nombre">
                            Introduce tu nombre: 
                        </td>
                    <td>
                        <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
                </label> 
                    </td>
                </tr>
  
                    
                <tr>  
                    <td>
                        <label for="color">
                            Introduce tu color favorito:
                        </td>
                    <td>
                        <input type="text" name="color" id="color" placeholder="Introduce tu color favorito">
                </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="altura">
                            Introduce tu altura:
                        </td>
                    <td>
                        <input type="text" name="altura" id="altura" placeholder="Introduce tu altura">
                </label> 
                    </td>
                </tr>
            
                <tr>  
                    <td><label for="peso">
                            Introduce tu peso:
                    </td>
                <td>
                    <input type="text" name="peso" id="peso" placeholder="Introduce tu peso">
                </label> 
                        </td>
                </tr>
            
            </table>  
            <br>
            <button>Voy a tener suerte</button>
        </form>
    </body>
</html>
