<!DOCTYPE html>
<!--
Recogemos los datos enviados desde la página ejercicio 1 con el metodo abreviado y creamos una tabla.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table width="350"border="2">
            <tr>
                <td>Nombre: </td>
                <td><?= $_GET["nombre"];?></td>
            </tr>
            <tr>
                <td>Color: </td>
                <td><?= $_GET["color"];?></td>
            </tr>
            <tr>
                <td>Alto: </td>
                <td><?= $_GET["altura"];?></td>
            </tr>
            <tr>
                <td>Peso:</td>
                <td><?= $_GET["peso"];?></td>
            </tr>
        </table>
          
    </body>
</html>
