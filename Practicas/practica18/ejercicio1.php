<?php


spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

use ejercicio1\Vehiculo;
use ejercicio1\Camion;
use ejercicio1\Autobus;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
    </head>
    <body>
        
        <div align='center'>
        <?php
        
        
        $camion = new Camion();
        $camion->encender();
        $camion->cargar(10);
        $camion->verificar_encendido();
        $camion->matricula = 'MDU - 293';
        $camion->apagar();
        $autobus = new Autobus();
        $autobus->encender();
        $autobus->subir_pasajeros(5);
        $autobus->verificar_encendido();
        $autobus->matricula = 'KDF -293';
        $autobus->apagar();      

        
        ?>
            
        </div>    
    </body>
</html>
