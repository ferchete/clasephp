<?php


namespace ejercicio1;

/**
 * Description of Camion
 *
 * @author Programacion
 */
class Camion extends Vehiculo {
    private $carga;
    
    public function cargar($cantidad_a_cargar){
        $this->carga = $cantidad_a_cargar;
        echo 'se ha cargado la cantidad ' . $cantidad_a_cargar . '<br />';
    }
    
    public function verificar_encendido() {
        if($this->encendido == true){
            echo 'Camion encendido <br />';
        }else{
            echo 'Camion apagado <br />';
        }
    }
    
}
