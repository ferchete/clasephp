<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
use ejercicio2\Persona;
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 2</title>
    </head>
    <body>
        <?php
        $padre = new Persona("Fernando", "Abramo", 34);
        $hijo=$padre;
        $hija=clone $padre;
        $hijo->setEdad(100);
        $hija->setEdad(50);
        var_dump($hijo);
        var_dump($hija);
        var_dump($padre);
        ?>
    </body>
</html>
