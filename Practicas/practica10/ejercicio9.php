<?php

/*Comprobamos si introducimos numero o no, si no introducimos numero nos sale un aviso en el campo "obligatorio"
 * Creamos unos estilos con la función "style" 
 *En caso de introducir numeros nos sale un var_dump con los datos que hemos introducido y su tipo
 * 
 */
if(!empty($_REQUEST)){
    if($_REQUEST["numero"] > 0){
        $caso = "bien";
        }else{
            $caso = "mal";
        }
}else{
    $caso = "mal";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 9</title>
        <style type="text/css">
            input[type="number"]{
                width: 300px;
            }
            .obligatorio::before{
                content:"Obligatorio";
                min-width: 150px;
                display: inline-block;
            }
            .noObligatorio::before{
                content:"Opcional";
                min-width: 150px;
                display:inline-block;
            }
        </style>
    </head>
    <body>
        <?php
        if($caso == "bien"){
            var_dump($_REQUEST);
        }else{
        ?>
        <div>
            <form name="f">
                <div class="obligatorio"><input required placeholder="Introduce un numero" step="1" min="1" max="100" type="number" name="numero"/></div>
                <div class="noObligatorio"><input placeholder="Introduce otro numero" step="1" min="1" max="100" type="number" name="numero1"/></div>
                <input type="submit" value="Enviar" name="boton" />
            </form>
        </div>        
        <?php
        }
        ?>
    </body>
</html>















