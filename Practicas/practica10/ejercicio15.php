<!DOCTYPE html>
<?php
//Recogemos cualquier dato del formulario mediante el metodo REQUEST

    define("ruta","http://localhost/poo2021/php/Practicas/practica10/");
    if(!$_REQUEST){
        header("Location: " . ruta . "/ejercicio5.php");//Le decimos que nos redireccione al ejercicio.php si no le damos a enviar
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        extract($_REQUEST); //importamos las variables a la tabla de simbolos desde un array
        echo $nombre; //Mostramos el valor introducido en el formulario
        
        //Si le damos al boton 
        ?>
    </body>
</html>
