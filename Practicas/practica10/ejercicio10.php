<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo __FILE__; //Constante predefinida que muestra la ruta del archivo
        echo "<br>"; //Salto de linea
        echo __LINE__;//Constante predefinida que muestra el numero de linea actual del fichero
        echo "<br>";//Salto de linea
        echo PHP_VERSION;//Constante predefinida 
        echo "<br>";//Salto de linea
        echo PHP_OS;//Constante predefinida que muestra el sistema operativo para el que se construyó PHP
        echo "<br>";//Salto de linea
        echo __DIR__;//Constante predefinida que muestra la dirección de un ficheros
        echo "<br>";//Salto de linea
        ?>
    </body>
</html>
