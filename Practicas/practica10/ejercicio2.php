<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        //Inicializamos las variables  y creamos los arrays
        $datos = array(20, 40, 790, 234, 890);
        $todo = max($datos);
        //Con la función max, cogemos el mayor número del array
        
        //Recorremos el array
        foreach ($datos as $key => $value) {
            $datos[$key] = (int) (100 * $datos[$key] / $todo);
        }
        //Comprobamos que le llegan los datos y mostramos los valores
        var_dump($datos);
        
       /*
        * Volvemos a recorrer el array  y mostramos los asteriscos 
        * dependiendo de los valores del array  
        */
        
        
        foreach ($datos as $value) {
            for ($c = 0; $c < $value; $c++) {
                echo "*";
            }
            echo "<br>";
        }
        ?>
    </body>
</html>
