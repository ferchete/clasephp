<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 3</title>

        <!--
        Creamos los estilos para utilizarlos después
        -->
        <style>

            *{
                margin: 0px;
                padding: 0px;
            }

            span{
                width: 10px;
                height: 50px;
                background-color: #001ee7;
                display: inline-block;
            }
            div{
                height: 50 px;
                display:inline-block;
                border: 5px solid #CCC;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <?php
        //Inicializamos las variables y arrays
        $datos = array(20, 40, 790, 234, 890);
        $todo = max($datos);
        //Recorremos el array
        foreach ($datos as $key => $value) {
            $datos [$key] = (int) (100 * $datos[$key] / $todo);
        }

        //Mostramos los datos que recogemos del array
        var_dump($datos);

        //Con los estilos arriba creados dibujamos unas barras a diferencia de los asteriscos puestos en el ejercicio anterior

        foreach ($datos as $value) {
            echo "<div>";
            for ($c = 0; $c < $value; $c++) {
                echo "<span></span>";
            }
            echo"</div>";
            echo "<br>";
        }
        ?>
    </body>
</html>
