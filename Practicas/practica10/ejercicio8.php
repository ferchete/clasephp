<?php

/*
 * Creamos las variables y comprobamos si hemos pulsado o no el boton
 * Si instroducimos un valor superior a 0, nos muestra un var_dump con los datos recogidos
 * Si es negativo, nos devuelve al formulario
 */



if(!empty($_REQUEST)){
    if($_REQUEST["nombre"] > 0){
        $caso="bien";
    }else{
        $caso="mal";
    }
}else{
    $caso = "mal";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 8</title>
    </head>
    <body>
        <?php
        //
        if($caso == "bien"){
            var_dump($_REQUEST);
        }else{
        ?>
        <div>
            <form name="f">
                Número: <input type="number" name="nombre" value="" />
                <input type="submit" value="Enviar" name="boton" />
            </form>
        </div>
        <?php
        }
        ?>
    </body>
</html>
