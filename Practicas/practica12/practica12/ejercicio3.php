<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 3</title>
    </head>
    <body>
        <?php
        $contador = 0;

        //Creamos las variables y el array para meter los resultados
        $sumaTiradas = [];

        //El bucle para realizar las tiradas y meter en el array los resultados

        for ($c = 0; $c < 10; $c++) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
            $sumaTiradas[] = $d1 + $d2;
            ?>
            <div>
                <div class="dados">
                    <img class="dados" src="imgs/<?= $d1 ?>.svg" alt="foto random"/>
                    <img class="dados" src="imgs/<?= $d2 ?>.svg" alt="foto random"/>
                </div>
                <div class="total">Total: <span> <?= $sumaTiradas[$contador++] ?> </span></div>
            </div>
    <?php
}
//Recogemos los datos con el var_dump
//var_dump($sumaTiradas);
?>
        <div class="mayor">

            La tirada mayor es <?= max($sumaTiradas) //Mostramos el resultado máximo de las tiradas?>

        </div>
    </body>
</html>
