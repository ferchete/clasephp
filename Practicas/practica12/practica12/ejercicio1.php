<?php
        
        $alto="400px"; //Creamos las variables para los estilos
        $ancho="400px"; 
        
        $dado1= mt_rand(1,6); //Con la función mt_rand, pedimos números aleatorios
        $dado2= mt_rand(1,6);
 
        
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
        
        <style> 
            .dados{
                width: <?= $ancho ?> px;
                height: <?= $alto ?>;
            }
            
            
            /*
            *Creamos los estilos 
            *poniendo un punto antes del estilo
            */
            
            
            .salida{ 
                border:1px solid black; 
                width:100px;
                height:110px;
                line-height: 40px;
                text-align: center;
                display:inline-block;
            }
            
            div.salida{
                color:blue;
                
            }

        </style>
    </head>
    <body>
        
        <div style="width: <?= (int) $ancho*2 ?> px"> 
            <img class="dados" src="imgs/<?=$dado1 //Lanzamos los dados ?>.svg" alt="foto random"/>
            <img class="dados" src="imgs/<?=$dado2 // Lanzamos el segundo dado?>.svg" alt="foto random"/>
        </div>
        <div class="salida"> 
        <?php
        
        echo "<h1>Total: " . $dado1+$dado2 . "<h1>"; //Sumamos las variables
        
        ?>
        </div>
    </body>
</html>
