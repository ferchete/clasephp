<!DOCTYPE html>

<?php

//Insertamos una subvista

require './_cabecera.php';

//Creamos los arrays, que cuando esta vacio es unidimensional

$tiradas = [];

$sumaTiradas = [];

//inicializamos las funciones

function tiradas(&$d1, &$d2) {

    $d1 = mt_rand(1, 6);

    $d2 = mt_rand(1, 6);
}

//Creamos un bucle para realizar las tiradas de dados 
//y rellene el array de tiradas

$c = 0;

while ($c < 10) {

//llamamos a la función para que nos tire los dados
    //Le decimos que es la primera fila del array

    tiradas($tiradas[$c][0], $tiradas[$c][1]);
    
    //Añadimos un nuevo elemento que es $sumaTiradas

    $sumaTiradas[] = $tiradas[$c][0] + $tiradas[$c][1];

    $c++;
}

//Realizamos un foreach para recorrer el array tiradas, $i (indice) y $v(valor)
    

foreach ($tiradas as $i => $v) {
    ?>


    <html>
        <head>
            <meta charset="UTF-8">


            <title>EJERCICIO 4</title>
        </head>
        <body>

            <div>

                <div class="dados">

                    <img src="imgs/<?= $v[0] ?>.svg" alt="dado1"/>

                    <img src="imgs/<?= $v[1] ?>.svg" alt="dado1"/>

                </div>

                <div class="total"> Total: <span><?= $sumaTiradas[$i] //?></span></div>

            </div>        

    <?php
}


//Decimos que nos de la suma de las tiradas

$tiradaMaxima = max($sumaTiradas);

echo "<h1>La mayor tirada ha sido $tiradaMaxima </h1>";

// var_dump($tiradaMaxima);
?>
    </body>
</html>
