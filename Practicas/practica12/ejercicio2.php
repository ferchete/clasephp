<?php
        /*
         * Creamos las variables, para ver el alto y ancho del dado con 350px
         */


        $alto="350px";
        $ancho="350px";
        
        
        
        
        ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 2</title>
        <style>
            .dados{
                width: <?= $ancho ?> px;
                height: <?= $alto ?> ;
            }
            
            
            
            
            
            .salida{
                border:1px solid black;
                width:100px;
                height:110px;
                line-height: 40px;
                text-align: center;
                display:inline-block;
            }
            
            div.salida{
                color:blue;
                
            }

        </style>
    </head>
    <body>
        <?php
        
        //Creamos el bucle y el array para meter las tiradas aleatorias dde los dados
        
        for($contador=0;$contador<10;$contador++){
                 $dado1= mt_rand(1,6);
                 $dado2= mt_rand(1,6);
                 
                 //Introducimos las tiradas en el array, de cada bucle del for
                 //array_push($tiradas, $dado1 + $dado2) -> te mete en el primer hueco libre de tiradas los datos
                 
                 $tiradas[]=$dado1+$dado2;
                 
                 require './_tirada.php';
        ?>
        
        <div style="width: <?= (int) $ancho*2 ?> px">
            <img class="dados" src="imgs/<?=$dado1 ?>.svg" alt="foto random"/>
            <img class="dados" src="imgs/<?=$dado2 ?>.svg" alt="foto random"/>
        </div>
        <div class="salida"> 
        <?php
        
        //Sumamos las tiradas de los dados
        
        echo "<h1>Total: " . $dado1+$dado2 . "</h1>";
        }
        
        //var_dump($tiradas);
        ?>
        </div>
        <div>La tirada máxima <?= max($tiradas)?></div>
    </body>
</html>
