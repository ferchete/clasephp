<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 3_1</title>
    </head>
    <body>
        <?php
        $tiradas = [];
        $sumaTiradas = [];

        function tirada(&$d1, &$d2) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
        }

        for ($c = 0; $c < 10; $c++) {

            tirada($tiradas[$c][0], $tiradas[$c][1]);

            $sumaTiradas[] = $tiradas[$c][0] + $tiradas[$c][1];
        }

        foreach ($tiradas as $i => $v) {
            ?>
            <div>
                <div class="dados">

                    <img src="imgs/<?= $v[0] ?>.svg" alt="dado1"/>

                    <img src="imgs/<?= $v[1] ?>.svg" alt="dado1"/>

                </div>
                <div class="total">Total: <span><?= $sumaTiradas[$i] ?></span></div>
            </div>
            <?php
        }

        // var_dump($tiradas)
        ?>
    </body>
</html>
