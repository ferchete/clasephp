<!DOCTYPE html>
<?php
include_once 'Vehiculo.php';
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 6</title>
    </head>
    <body>
        <?php
        $ford = new Vehiculo ("DHH2323", "rojo", false);
        //Para llamar a un metodo estático puedo utilizar
        $ford -> mensaje();//forma 1 (no es aconsejable)
        Vehiculo::mensaje();//forma 2 -> forma recomendada
        $ford::mensaje(); // forma 3 (No es aconsejable)
        //Muestro todas las propiedades del nuevo objeto
        var_dump($ford);
        //No puedo acceder a una propiedad estática de esta forma
        //echo $ford -> ruedas(); Nos dará error
        echo $ford::$ruedas; //Así puedo acceder a una propiedad estática porque $ford es un objeto
        //
        //para acceder a una propiedad estática
        Vehiculo::$ruedas; //leyendo la propiedad Es la recomendable
        //Cuando tiene parentesis() es metodo y con $ propiedad
    
       
        Vehiculo::$ruedas = 6; //Escribiendo la propiedad
        
       // Vehiculo::$matricula;//LA matricula no es un elemento estático, por eso hay que acceder de esta manera
        
        
        
       // Vehiculo::encender(); //Esta llamada es correcta, pero el metodo está mal creado
       /*
        * Concretando VOL.1:
        * 
        * Para acceder a propiedades estáticas
        * NombreClase::$nombrePropiedad
        * $nombreObjeto::$nombrePropiedad
        */
        
        /*
         * Concretando VOL.2:
         * 
         * Para llamar a metodos estáticos
         * NombreClase::NombreMetodo();
         * $nombreObjeto::NombreMetodo();
         */
        
        /*
         * Concretando VOL.3:
         * 
         * Si quiero acceder a miembros no estáticos       
         * $nombreObjeto -> NombrePropiedad;
         * $nombreObjeto -> NombreMetodo();
         */
        
        echo "<br>" . Vehiculo::$ruedas; 
        
        ?>
    </body>
</html>
