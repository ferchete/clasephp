<?php


class Vehiculo {
    
    public $matricula;
    private $color;
    protected $encendido;
    public static $ruedas = 5; //propiedad de la clase
    
    function __construct($matricula, $color, $encendido) {
        $this -> matricula = $matricula;
        $this -> color = $color;
        $this -> encendido = $encendido;
    }
    //metodo de la clase
    public static function encender(){
        $this -> encendido = true;
        echo 'Vehiculo encendido <br> ';
    }
    
    public function apagar(){
        // No puedes poner $this-> dentro de metodos estáticos
        // $this -> encendido = false; 
        echo 'Vehiculo apagado <br /> ';
    }
    public static function mensaje(){
        echo "Este es mi coche <br>";
    }
    public static function ruedas(){
        //Los metodos estáticos solo pueden acceder a propiedades y metodos estáticos
        echo Vehiculo::$ruedas; //forma 1
        echo self::$ruedas; // forma 2
    }
}
?>
