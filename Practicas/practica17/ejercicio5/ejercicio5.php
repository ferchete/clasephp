<!DOCTYPE html>
<?php
//Llamamos a la clase
    require 'Vehiculo.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //isntancio un objeto de tipo Vehiculo 
        //Y además le estoy inicializando
        $ford = new Vehiculo ("DHH2323", "rojo", false);
        var_dump($ford);
        $ford -> apagar();
        //Instancio el objeto de tipo vehiculo
        $renault = new Vehiculo();
        var_dump($renault);
        ?>
    </body>
</html>
