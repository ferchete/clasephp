<!DOCTYPE html>
<?php

/*
 * Clase usuario
 */
class Usuario {
    
    //propiedades
    
    public $nombre = "defecto";
    private $edad;
    protected $telefono;
    private $apellidos = "defecto";
    private $nombrecompleto;
    
    
    //getters
    public function getNombreCompleto(){
        return $this-> nombrecompleto;
    }
    public function getApellidos(){
        return $this -> apellidos;
    }   
    public function getNombre(){
        return $this->nombre;
    }
    public function getEdad(){
        return $this-> telefono;
    }
    
    //setter
    public function setNombre ($nombre){
        $this -> nombre = $nombre;
        $this -> concatenar();
    }
    public function setEdad($edad){
        $this-> edad = $edad;
    }
    public function setTelefono ($telefono){
            $this-> telefono = $telefono;
    }
    public function setApellidos($apellidos) {
        $this -> apellidos = $apellidos;
        $this -> concatenar ();
    }
    
    // metodo privado
    
    
    /**
     * Asignando a la propiedad nombre completo el valro del nombre y los apellidos
     */
    private function concatenar() {
        $this -> nombreCompleto = $this -> nombre . " " . $this-> apellidos;
    }
}




?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <?php
        
        /** crear el objeto */
        
        $persona = new Usuario();
        
        
        echo $persona->nombre; // Leo la propiedad nombre porque es publica ("Defecto")
        $persona->setEdad(51); // Asignando una edad con el setter
        $persona ->setTelefono("232323"); //Asigno un telefono con el setter
        $persona->setApellidos("vazquez rodriguez"); //Asigno los apellidos con el setter
        var_dump($persona);
        
        $persona->nombre = "Ramon"; //Asigno un nombre al objeto con la propiedad (al utilizar la propiedad no actualiza el nombre al completo)
        var_dump($persona);
        $persona->setNombre("Ramon"); //Asigno el nombre al objeto con el setter
        var_dump($persona);
        
        
        
        ?>
    </body>
</html>
