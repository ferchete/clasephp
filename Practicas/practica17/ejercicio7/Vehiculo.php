<?php

class Vehiculo {

    // propiedades
    public $matricula;
    private $color;
    protected $encendido;
    public $numero;

    
    /**
     * Este metodo se ejecuta automaticamente cuando instancias el objeto
     * Es un metodo magico de php
     * está leyendo cuantos argumentos le pasas
     */
    
    /**
     * Así sobrecargamos el constructor
     */
//    public function __construct() {
//        $this->numero=func_num_args();  // me indica el numero de argumentos pasados al constructor
//        if($this -> numero ==1){
//            $this-> matricula= func_get_arg(0);
//        }elseif ($this -> numero == 2) {
//            $this-> matricula = func_get_arg(0);
//            $this-> color = func_get_arg(1) ;
//            $this-> encendido = func_get_arg(2);
//                    
//        }
//        
//    }
    
    /**
     * Sobrecarga del constructor utilizando argumentos por defecto
     * @param type $matricula
     * @param type $color
     * @param type $encendido
     */
    public function __construct($matricula ="", $color="", $encendido=false) {
        
    }

    public function encender() {
        $this->encendido = true;
        echo 'Vehiculo encendido <br />';
    }

    public function apagar() {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }

}


