<!DOCTYPE html>
<?php
/*
 * Clase Usuario
 */

class Usuario{
    
    // propiedades
    public $nombre = "defecto";
    private $edad; //solo es accesible desde la propia clase
    protected $telefono; // tampoco se puede acceder a ella desde la clase y desde los hijos
    
    //metodos
    
    //getter -> obtener
    // metodos para leer las propiedades
    public function getNombre(){
        return $this->nombre;
    }
    public function getEdad(){
        return $this-> edad;
    }
    public function getTelefono(){
        return $this-> telefono;
    }
    
    //setter
    //Escribir en las propiedades
    public function setNombre($nombre){
        $this -> nombre = $nombre;
    }
    public function setEdad($edad){
        $this -> edad = $edad;
    }
    public function setTelefono($telefono) {
        $this -> telefono = $telefono;
    }
}


/*
 * Vamos a crear objetos de tipo usuario
 */


$persona = new Usuario();

// Leyendo el nombr de la persona (defecto)
echo $persona -> nombre;

// Escribo en la propiedad edad 51
$persona -> setEdad(51);

//Escribo en la propiedad telefono
$persona ->setTelefono("123456789");

//Muestro todas las carácteristicas -> Vuelvo en pantalla los valores del objeto
var_dump($persona);

// Escribiendo el nombre de la persona sin utilizar setter
$persona -> nombre = "Silvia";
//Escribiendo la edad de la persona sin utilizar el setter
//nos dará error porque accedemos a ella como si fuese publica
//la solución sería 
//$persona -> setEdad(12); // Bien
$persona ->edad = 12; //Mal
//y en esta ocasión nos da error por el protected
//la solución sería
//$persona -> setTelefono("987654321"); //Bien
$persona ->telefono ="987654321"; //Mal
var_dump($persona);





?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 2</title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
    </body>
</html>
