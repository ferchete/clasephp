<!DOCTYPE html>
<?php
//Llamamos a la clase Coche que está alojado en otro archivo
include "Coche.php";
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $coche = new Coche();//Instanciamos la clase Coche
        $coche -> color = 'Rojo'; // Llenamos algunas de las propiedades
        $coche -> marca = 'Honda';
        $coche -> numero_puertas = 4;
        $coche -> llenarTanque(10); //echamos gasolina (litros)
        var_dump($coche);
        
        echo $coche ->acelerar(); // Aceleramos el coche y mostramos la gasolina que queda
        
        $coche -> acelerar();
        $coche -> acelerar();
        $coche -> acelerar();
        var_dump($coche);
             
        
        ?>
    </body>
</html>
