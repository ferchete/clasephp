<?php

Class Coche {
    public $color;
    public $numero_puertas;
    public $marca;
    public $gasolina = 0;
    
    
    //Metodo publico para echar gasolina al coche
    function llenarTanque ($gasolina_nueva){
        $this -> gasolina = $this-> gasolina + $gasolina_nueva;
    }
    
    // metodo publico parar acelerar
    //Este metodo comprueba si te queda gasolina
    //Si hay gasolina le resta un litro y te devuelve cuanta gasolina te queda
    function acelerar() {
        if ($this -> gasolina > 0){
            $this -> gasolina = $this->gasolina -1;
            return 'Gasolina restante ' . $this-> gasolina;
        }
    }
}

?>