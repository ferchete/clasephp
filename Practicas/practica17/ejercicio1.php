<!DOCTYPE html>
<?php

/**
 * Clase de tipo usuario
 * Clase para probar metodos y propiedades
 */



class Usuario {
    // Definiendo miembros
    
    //propiedades
    public $nombre ="defecto"; //propiedad publica con valor predeterminado y de tipo string
    
    // metodos
    public function setNombre($_nombre = "Fernando"){ //metodo publico setter, porque me permite introducir un valor en la propiedat
        $this->nombre=$_nombre;
    }
    public function getNombre(){ //metodo publigo (getter) nos permite leer el valor de la propiedad nombre
        return $this-> nombre;
    }
}
    
 /*
  * Creando un obejeto de tipo Usuario 
  * Utilizando directamente la propiedad nombre
  */   


$persona = new Usuario(); //Persona es un objeto de tipo Usuario
echo $persona->nombre; //Lleyendo la propiedad nombre de persona (defecto)
$persona->nombre = "Silvia"; //Escribiendo en la propiedad nombre el valor Silvia
var_dump($persona);

//Creamos otro objeto de tipo Usuario
//Utilizar los getter y los setter para acceder a la propieda nombre

$persona1 = new Usuario(); 
echo $persona1 ->nombre;//Leo el valor por defecto de nombre que es "defecto"
$persona1 ->setNombre(); //Asigno el valor "Fernando" a nombre
echo $persona1->setNombre(); //leo el valor de nombre (Fernando)




?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1</title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
    </body>
</html>
