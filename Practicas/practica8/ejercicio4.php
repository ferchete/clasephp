<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 4</title>
    </head>
    <body>
        <form action="ejercicio4Destino.php">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">      
            </div>
            <br>
            <div>
                <label for="email">
                    E-mail:
                </label>
                <input type="email" id="correo" name="email" placeholder="Introduce tu correo">
            </div>
            <br>
            <div>
                <button formethod="GET">Enviar por GET</button>
                <button formethod="POST">Enviar por POST</button>
            </div>
        </form>
        
    </body>
</html>
