<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJERCICIO 1 DESTINO</title>
    </head>
    <body>
        <!-- Mostramos los datos utilizando el metodo largo -->
        
        
        ¡Hola <?php echo $_GET["nombre"]; ?>!<br>
        ¡Tu email es: <?php echo $_GET["email"]; ?>!<br>
        
        
        <br><br>
        
        <!-- Mostramos los datos utilizando el metodo corto -->
        
        ¡Hola <?= $_GET["nombre"]; ?>!<br>
        ¡Tu email es: <?= $_GET["email"]; ?>!<br>     
          
        
        
        
        
    </body>
</html>
