<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <div>Es texto escrito directamente en HTML</div>
        <?php
        // comentario en linea
        echo "texto escrito en PHP<br>";
        /*
         * comentario en varias lineas  
         */
        print "texto escrito en PHP";
        ?>
        
        <h1>Segunda parte</h1>
        <?php
        #comentario en una linea
        $a=10; #Variable de tipo entero
        
        echo "escribir en PHP para colocar el valor de la variable $a";        
              
        ?>
    </body>
</html>
