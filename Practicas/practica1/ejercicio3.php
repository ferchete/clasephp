<!DOCTYPE html>
<!--
Ejercicio2
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 3</title>
    </head>
    <body>
        <h2>Método 1</h2>
        <?php
        // mezclamos PHP y HTML
        echo "<p>Hola mundo</p>";
        ?>
        <h2>Método 2</h2> 
        <p>
            <?php
            //sólo coloco PHP
            echo"hola mundo";
            ?>
            
        </p>   
        <h2>Método 3</h2> 
        
        <?php
        echo "<p>";
        ?>
        Hola mundo
        <?php
        echo "</p>"
        ?>
        
        <h2>Podemos utilizar el metodo contraido del echo </h2>
        
        <p>
            <?= "hola mundo" ?>
        </p>
        
    </body>
</html>
