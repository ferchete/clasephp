<?php

/**
 * Description of Circulo
 *
 * @author Programacion
 */

//Area  = pi() * r**2
//Perimetro = 2 * pi() * r

namespace ejemplo9;

class Circulo {
    private $radio;
    private $centrox;
    private $centroy;

    
    
    public function __construct($radio=50, $centrox=25, $centroy=25) {
        $this->radio = $radio;
        $this->centrox = $centrox;
        $this->centroy = $centroy;
    }
    public function getRadio() {
        return $this->radio;
    }

    public function getCentrox() {
        return $this->centrox;
    }

    public function getCentroy() {
        return $this->centroy;
    }

    public function setRadio($radio) {
        $this->radio = $radio;
    }

    public function setCentrox($centrox) {
        $this->centrox = $centrox;
    }

    public function setCentroy($centroy) {
        $this->centroy = $centroy;
    }
    
    public function area(){
        $area = 0; // Inicializamos y declaramos
        $area = pi() * $this->getRadio()**2;
        //             $this->radio
        return $area;
    }
    
    public function perimetro() {
        $perimetro = 0; // Inicializamos y declaramos
        $perimetro = 2 * pi() * $this->getRadio();
        //                      $this->radio
        return $perimetro;
    }

    public function pintar() {
        
        echo '<svg height="500" width="500">';
        echo '<circle cx="'. $this->centrox.'" cy="'.$this->centroy.'" r="'.$this->radio.'" stroke="black" stroke-width="3" fill="red">';
        echo '</svg>'; 
        
    }
    
}
