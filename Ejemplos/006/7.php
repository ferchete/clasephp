<!DOCTYPE html>
<?php
    require_once './ejemplo7/Animales.php';
    require_once './ejemplo7/Ciudad.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // quiero utilizar la clase
        //Creamos un objeto de tipo animal
        
        $animal1 = new Animales();
        
        $animal1 -> setNombre("miau");
        $animal1 -> setRaza("Siames");
        $animal1 -> setColor("Gris");
        $animal1 -> setFechaNacimiento("2021/01/01");
        
        //Si queremos mostrar la información introducida
        
        echo $animal1 ->getNombre(). "<br>";
        echo $animal1 ->datos();
        
        $animal2 = new Animales();
        
        $animal2 -> setNombre("guau");
        $animal2 -> setRaza("mastin");
        $animal2 -> setColor("negro");
        $animal2 -> setFechaNacimiento("2021/01/01");
                
        echo $animal2 ->getNombre() . "<br>";
        echo $animal2 ->datos();
       
        $ciudad1 = new Ciudad();
        $ciudad1 -> set_nombre("Santander");
        $ciudad1 -> set_provincia("Cantabria");
        
        echo $ciudad1 ->get_nombre() . " y su provincia es ";
        echo $ciudad1 ->get_provincia() . "<br>";
        echo "<div>" . $ciudad1 ->obtenerIniciales(mt_rand(1,5)) . "</div>";
        
        $ciudad2 = new Ciudad();
        $ciudad2 -> set_nombre("Bilbao");
        $ciudad2 -> set_provincia("Euskadi");
        
        echo $ciudad2 -> get_nombre() . " y su provincia es ";
        echo $ciudad2 -> get_provincia();   
        echo "<div>" . $ciudad2 ->obtenerIniciales(mt_rand(1,5)) . "</div>";
        
        
        
        
        ?>
    </body>
</html>
