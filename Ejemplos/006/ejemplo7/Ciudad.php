<?php

/**
 * Description of Ciudad
 *
 * @author Programacion
 */
class Ciudad {
    
    //propiedades
    public $_nombre;
    private $_provincia; 
    
    
    //metodos      
    

    public function get_nombre() {
        return $this->_nombre;
    }

    public function get_provincia() {
        return $this->_provincia;
    }

    public function set_nombre($_nombre) {
        $this->_nombre = $_nombre;
    }

    public function set_provincia($_provincia) {
        $this->_provincia = $_provincia;
    }

    public function obtenerIniciales($numero) {
        
        $resultado = substr($this->_nombre,0,$numero) . " " . substr($this->_provincia,0,$numero);
        return $resultado;
        //return substr($this->_nombre,0,1) .  ". " . substr($this->_provincia,0,1) . ". ";
        
    }
    
    
}
