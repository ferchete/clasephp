<?php

/**
 * Description of Animales
 *
 * @author Programacion
 */
class Animales {
    //Miembros 
    public $nombre;
    private $raza;
    protected $color;
    public $fechaNacimiento;
    
    
    //metodos que son las funciones
    public function getNombre() {
        return $this->nombre;
    }

    public function getRaza() {
        return $this->raza;
    }

    public function getColor() {
        return $this->color;
    }

    public function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    public function setNombre($nombre)  {
        $this->nombre = $nombre;
    }

    public function setRaza($raza)  {
        $this->raza = $raza;
    }

    public function setColor($color)  {
        $this->color = $color;
    }

    public function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    public function datos() {
        
        $resultado = "<ul>";
        $resultado =$resultado . "<li>{$this ->nombre}</li>";
        $resultado =$resultado . "<li>{$this ->raza}</li>";
        $resultado =$resultado . "<li>{$this ->color}</li>";
        $resultado .= "</ul>";
        
        
        return $resultado;
    }  
    
    
    
    
    
    
    

    
    
    
    
    
}
?>