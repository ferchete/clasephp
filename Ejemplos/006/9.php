<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});
use ejemplo9\Circulo
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>EJEMPLO 9</title>
    </head>
    <body>
        <?php
        
        // Radio 3,4
        // centroX 2,0
        // centroY 2,0
        
        $unCirculo = new Circulo();
        $unCirculo->setRadio(3.4);
        $unCirculo->setCentrox(20.0);
        $unCirculo->setCentroy(20.0);
        
        var_dump($unCirculo);
        
        echo "<h2> El radio es " . $unCirculo->getRadio() . "</h2>";
        echo "<h2> El centro en X es " . $unCirculo->getCentrox() . "</h2>";
        echo "<h2> El centro en Y es " . $unCirculo->getCentroy() . "</h2>";
        
        
        echo "<br> Area = " . $unCirculo->area();
        echo "<br> Petrimetro = " . $unCirculo->perimetro();
        $unCirculo->pintar();
        
        
        
        $unCirculo1=new Circulo(15,30,50);
        var_dump($unCirculo1);
        
        
        
        echo "<h2> El radio es " . $unCirculo1->getRadio() . "</h2>";
        echo "<h2> El centro en X es " . $unCirculo1->getCentrox() . "</h2>";
        echo "<h2> El centro en Y es " . $unCirculo1->getCentroy() . "</h2>";
        
        echo "<br> Area = " . $unCirculo1->area();
        echo "<br> Petrimetro = " . $unCirculo1->perimetro();
        
        $unCirculo1->pintar();
        
       
        
        
        ?>
    </body>
</html>
