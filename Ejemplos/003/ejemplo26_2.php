<?php

function dado() { //creo la funcion
    $numero = mt_rand(1, 6); //creo la variable con el mt_rand para numeros aleatorios
    $imagen = '<img src="./imgs/' . $numero . '.svg">';//le decimos las imagenes que queremos
    return $imagen;//que nos devuelva la variable imagen
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <?= dado() ?>
        <?= dado() ?>
        
        <?php
        $num=5;
        for($contador=0;$contador<$num;$contador++){
            echo dado();
        }
        ?>

    </body>
</html>
