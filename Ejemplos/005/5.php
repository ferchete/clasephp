<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <pre>
        Quiero un array bidimensional que almacene los clientes dados.
        
        codigo, nombre, edad
        
        1, Ana, 45
        2, Pedro, 35
        3, Luisa, 19
        
        volcamos todos los datos con un foreach

        </pre>

        <?php
        
        $persons =  [
                1 => ['ana', 45],
                2 => ['pedro', 35],
                3 => ['luisa', 19]
            ];

        
        foreach ($persons as $codigo => $datazo) {
            
            echo $codigo .  " " . $datazo[0] .  " " . $datazo[1];
      
         
        }
        
        ?>
    </body>
</html>
