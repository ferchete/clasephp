<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <pre>
        Quiero un array bidimensional que almacene los clientes dados.
        
        codigo, nombre, edad
        
        1, Ana, 45
        2, Pedro, 35
        3, Luisa, 19
        
        volcamos todos los datos con un foreach

        </pre>

        <?php
//        $persons =  [
//                1 => ['ana', 45],
//                2 => ['pedro', 35],
//                3 => ['luisa', 19]
//            ];

        $datos = [
            [
                "codigo" => 1,
                "nombre" => "Ana",
                "edad" => 45
            ],
            [
                "codigo" => 2,
                "nombre" => "Pedro",
                "edad" => 35
            ],
            [
                "codigo" => 3,
                "nombre" => "Luisa",
                "edad" => 19
            ],
        ];

//        $datos[0]["codigo"]=1;
//        $datos[0]["nombre"]="Ana";
//        $datos[0]["edad"]=45;
//        
//        $datos[1]["codigo"]=2;
//        $datos[1]["nombre"]="Pedro";
//        $datos[1]["edad"]=35;
//        
//        $datos[2]["codigo"]=3;
//        $datos[2]["nombre"]="Luisa";
//        $datos[2]["edad"]=19;
//        Leer edad de Pedro
//        echo $datos[1]["edad"];
                
        foreach ($datos as $registro) {
          echo "El codigo es "  . $registro["codigo"] . "<br>";
          echo "El nombre es "  . $registro["nombre"] . "<br>";
          echo "La edad es "  . $registro["edad"] . "<br>";
        }
        foreach($datos as $registro){
            //Este foreach recorre cada registro del grande
            foreach($registro as $campo => $valor){
                echo "{$campo}: {$valor} <br>";
            }
        }
        ?>
    </body>
</html>
