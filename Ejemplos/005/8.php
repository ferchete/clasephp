<!DOCTYPE html>
<!--
input output

-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        function calculArea($r) {

            $resultado = 0;
            $resultado = pi() * $r ** 2;
            return $resultado;
        }

        function calcularAreaReferencia($r, &$areaCalculada) {

            $areaCalculada = pi() * $r ** 2;
        }

        echo "<br> Area de un circulo <br>";
        $radio = 2;
        $area = 0;
        $area = calculArea($radio);
        var_dump($area);

        echo "<br> Area de un circulo por referencia <br>";
        $radio = 5;
        calcularAreaReferencia($radio, $area);
        var_dump($area);
        ?>
    </body>
</html>
