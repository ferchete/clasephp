<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>

        <pre>
            Crear un array denominado alumnos con los siguientes campos
            id, nombre, apellidos, nota
            Introducimos los siguientes registros
            1, Ana, Vazquez, 9
            2, Jose, Lopez, 6
            3, Luisa, Marcano, 9
            - Quiero que mostreis todos los registros 
            - Calcular la nota media

        </pre>
        <?php
         $alumnos = [
            [
                "id" => 1,
                "nombre" => 'Ana',
                "apellido" => 'Vazquez',
                "nota" => 9
            ],
            [
                "id" => 2,
                "nombre" => 'Jose',
                "apellido" => 'Lopez',
                "nota" => 6
            ],
            [
                "id" => 3,
                "nombre" => 'Luisa',
                "apellido" => 'Marcano',
                "nota" => 9
            ]
        ];

        foreach ($alumnos as $indice => $registro) {
            foreach ($registro as $campo => $valor) {

                echo "{$campo}: " . "{$valor} <br>";
            }
        }
        // $media = ($alumnos[0]['nota']+$alumnos[1]['nota']+$alumnos[2]['nota'])/3
        $media = 0;
        foreach($alumnos as $registro){
            $media = $media + $registro["nota"];
        }
        
        $media = $media / count($alumnos);
        
        echo "<h1>$media</h1>";
        ?>
    </body>
</html>
