
<?php

function operacionConRetorno($a, $b){
    //en una sola instrucción
    $c=[
        "suma" => $a+$b,
        "resta" => $a-$b,
        "prudcto" => $a*$b,
    ];
    
    return $c; //Devuelvo las operaciones
    
   /*En varias instrucciones
    *  $c["suma"]=$a+$b;
    * $c["resta"]=$a-$b;
    * $c["producto"]=$a*$b;
    */
}

function operacionReferencia ($a, $b, &$c) {
    //inicializando c y almacenando los resultados
       $c=[
        "suma" => $a+$b,
        "resta" => $a-$b,
        "prudcto" => $a*$b,
    ]; 
}
    


$numero1 = 20;

$numero2 = 10;

$resultado = [];

$resultado = operacionConRetorno($numero1, $numero2);

var_dump ($resultado);

$resultado = [];

operacionReferencia($numero1, $numero2, $resultado);

var_dump($resultado);



?>
