<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <pre>
Realizar una funcion que calcule la moda de un array pasado como argumentos
el array utilizado es $x=[,1,2,3,4,1,2,3,4,1,1];<!-- La moda que deberia devolver es 1 -->
        la sintaxis de la funcion:
        int  moda(array a utilizar)
        </pre>
        <?php

        function moda($vector) {

//            $salida = 0;
//
//            foreach ($vector as $val) {
//
//                if ($val == $vector) {
//
//                    $salida++;
//                }
//            }
//           
//            return $salida;
            
            $repeticiones= array_count_values($vector);
            $repeticionMaxima=max($repeticiones);
            $resultado = array_search($repeticionMaxima, $repeticiones);
            return $resultado;
        } 
        
        function moda1($vector) {

            $repeticiones = array_count_values($vector);
            $modaCalculada = 0;
            $repeticionesMaximas=0;
            foreach($repeticiones as $indice => $valor){
                if($valor > $repeticionesMaximas){
                    $repeticionesMaximas = $valor;
                    $modaCalculada = $indice;
                    
                }
            }
            return $modaCalculada;
        }
        
        
        

        $x = [1, 2, 3, 4, 1, 2, 3, 4, 1, 1];
        $salida=0;
        $salida = moda($x);
        var_dump($salida);
        
        
        echo "<br> Función hecha con foreach";
        
        $salida = moda1($x);
        var_dump($salida);
        
        ?>
    </body>
</html>
