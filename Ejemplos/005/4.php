
<?php

/*  function caja(){
  return '<rect x="0" y="0" width="100" height="100" >';
  }
  echo "<svg>";
  echo caja();
  echo "</svg>";
 * 
 */

function operacion($a, $b, $c) { //Si le ponemos el & nos devuelve 30, al asociarlo con $resultado
    
    $c = $a + $b; //La suma que realizo no sirve para nada
}


function operacionConRetorno($a, $b){
    
    $c=$a+$b;
    
    return $c; //Devuelvo la suma
}

function operacionReferencia (int $a, int $b, int &$c) {
    
        $c = $a + $b; //Devuelve la suma mnediante el &
        
        //Argumento por referencia "&" lo que cambia a $c, cambia a $resultado
}
    


$numero1 = 20;

$numero2 = 10;

$resultado = 0;

operacion($numero1, $numero2, $resultado); //$a=20|$b=10|$c=0

echo $resultado; //Resultado muestra 0, porque no le afecta lo de $c, así que vale 0

$resultado = operacionConRetorno($numero1, $numero2);

echo $resultado;

operacionReferencia($numero2, $numero1, $resultado);

echo $resultado;


?>
