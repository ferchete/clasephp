<?php

//Crear un array vacío
$a=[]; //Actual
#Antiguamente se hacía así, creando una función.
$b=array();
//Crear un array con elementos
$c=[2,21,3,"ejemplo"]; //a este tipo de array se le llama array enumerado, porque el array como indice tiene numeros
var_dump($c);
#array asociativo
$d=[
    "nombre"=>"fer",
    "edad"=>21,
    "poblacion"=>"pastillero"
];

var_dump($d);
#Leer una posición del array
echo $c[3];



echo $d["poblacion"];

//modificar posición del array
#En el array D, quiero cambiar EJEMPLO, por EJEMPLOS

$c[3]="ejemplos";

//modificar la edad a 50 en el array D

$d["edad"]=50;

//añadir elementos en el array 
#colocar un elemento en el final del array C

$c[]=100;

var_dump($c);
var_dump($d);

#Colocar nuevo elemento al final del array C, con PUSH
array_push($c, 350);
var_dump($c);

#colocar un elemento en la posición 50 del array C

$c[50]=100000;
var_dump($c);

#colocar un elemento nuevo en el array D

$d["peso"]=85;
var_dump($d);

//Crear un array de 2 dimensiones

$alumnos=[
    ["nombre"=>"Ivan",
        "edad"=>24],
    
    ["nombre"=>"Eva",
        "edad"=>25],
];

var_dump($alumnos);

//mostrar la edad de Eva

echo $alumnos[1]["edad"];





#array bidimensional

$numeros=[
    
    "pares"=>[2,4,8,10],
    
    "impares"=>[3,5,7,9],
    
];
       
var_dump($numeros);

echo $numeros["pares"][2];// Me muestra el numero 8



?>