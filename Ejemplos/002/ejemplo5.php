<?php
     $nombre="Benito";
     $edad=34;
     $poblacion="Santander"; //metodo recortado
     
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th>Enunciado</th>
                    <th>Resultado</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Nombre</td>
                    <td> 
                        <?php echo $nombre; ?>
                    </td>
                </tr>
                <tr>
                    <td>Edad</td>
                    <td><?php echo $edad; ?></td>
                </tr>
                <tr>
                    <td>Población</td> <!-- metodo abreviado, recomendable -->
                    <td><?= $poblacion ?></td> 
                    
                </tr>
            </tbody>
        </table>

    </body>
</html>
